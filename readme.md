# Password Chrome extension

## Realisation 
- Jacques Cornat

##Description
Because Chrome internal password manager **doesn't** keep safe your passwords, this small extension allow you to encypt each of your passwords with a master passphrase.
*Securize your passwords for sure*.

##TODO

 - Form validation
 
 - Search on manage page
 
 - Export/Import passwords
 
 - Error in manage page if no passwords
 
 - Edit passwords
 
 - Remove passwords
 
 - Password generation