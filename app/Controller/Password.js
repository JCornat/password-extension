(function() {

    var app = angular.module('app-password', ['app-service']);

    app.controller('PasswordAddCtrl', ['$scope', '$location', 'storageService', 'commonService', function ($scope, $location, storageService, commonService) {

        $scope.password = {};
        $scope.submitting = false;

        $scope.storageService = storageService;

        $scope.passwordType = 'password';
        $scope.$watch('displayPassword', function(val) {
            if (angular.isDefined(val)) {
                $scope.passwordType = (val)?'text':'password';
            }
        });

        $scope.passphraseType = 'password';
        $scope.$watch('displayPassphrase', function(val) {
            if (angular.isDefined(val)) {
                $scope.passphraseType = (val)?'text':'password';
            }
        });

        $scope.$watchGroup(['password.password', 'password.passphrase'], function(value) {
            if (angular.isDefined(value[0]) && angular.isDefined(value[1])) {
                $scope.password.hash = CryptoJS.AES.encrypt($scope.password.password, $scope.password.passphrase).toString();
            }
        });

        $scope.getCurrentSite = function() {
            chrome.tabs.getSelected(null, function(tab) {
                $scope.$apply(function() {
                    $scope.password.website = commonService.getDomainName(tab.url);
                });
            });
        };

        $scope.selectHash = function() {
            $('#hash').select();
        };

        $scope.submit = function() {
            $scope.submitting = true;
            storageService.add($scope.password, function(id) {
                $scope.$apply(function() {
                    $location.path('/');
                });
            });
        };

    }]);

    app.controller('PasswordUpdateCtrl', ['$scope', '$location', 'storageService', 'commonService', '$routeParams', function ($scope, $location, storageService, commonService, $routeParams) {

        $scope.password = {};
        $scope.submitting = false;

        storageService.find($routeParams.id, function(password) {
            $scope.$apply(function() {
                $scope.password = password.data;
            });
        }, function() {
            console.log("ERROR");
        });

        $scope.passwordType = 'password';
        $scope.$watch('displayPassword', function(val) {
            if (angular.isDefined(val)) {
                $scope.passwordType = (val)?'text':'password';
            }
        });

        $scope.passphraseType = 'password';
        $scope.$watch('displayPassphrase', function(val) {
            if (angular.isDefined(val)) {
                $scope.passphraseType = (val)?'text':'password';
            }
        });

        $scope.$watchGroup(['password.password', 'password.passphrase'], function(value) {
            if (angular.isDefined(value[0]) && angular.isDefined(value[1])) {
                $scope.password.hash = commonService.encrypt($scope.password.password, $scope.password.passphrase);
            }
        });

        $scope.getCurrentSite = function() {
            chrome.tabs.getSelected(null, function(tab) {
                $scope.$apply(function() {
                    $scope.password.website = commonService.getDomainName(tab.url);
                });
            });
        };

        $scope.selectHash = function() {
            $('#hash').select();
        };

        $scope.submit = function() {
            $scope.submitting = true;
            storageService.add($scope.password, function(id) {
                $scope.$apply(function() {
                    $location.path('/');
                });
            });
        };

    }]);

    app.controller('PasswordManageCtrl', ['$scope', '$location', 'storageService', function ($scope, $location, storageService) {

        $scope.a = [];
        $scope.passwordList = [];

        storageService.findAll(function(data) {
            $scope.passwordList = data;
        });

        $scope.detail = function(id) {
            $location.path('/password/'+id+'/detail');
        };

        $scope.toggleDisplayPassphrase = function(e, password) {
            password.displayPassphrase = !password.displayPassphrase;
            if (password.displayPassphrase) {
                $scope.$watchCollection('password', function(a) {
                    console.log(a);
                });
                setTimeout(function() {
                    $(e.target).closest('tr').find('input[type=password]')[0].focus();
                }, 100);
            }
        };

    }]);

    app.controller('PasswordDeleteCtrl', ['$scope', '$location', 'storageService', function ($scope, $location, storageService) {


    }]);

    app.controller('PasswordGenerateCtrl', ['$scope', '$location', 'storageService', function ($scope, $location, storageService) {


    }]);

})();
