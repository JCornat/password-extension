(function() {

    var app = angular.module('app-layout', ['app-directive']);

    app.controller('MenuCtrl', ['$scope', '$rootScope', function ($scope, $rootScope) {

    }]);

    app.controller('BackCtrl', ['$scope', '$rootScope', '$location', function ($scope, $rootScope, $location) {
        $scope.back = function() {
            $location.path($scope.url);
        };
    }]);

})();
