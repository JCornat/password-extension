(function() {

    var app = angular.module('app-directive', []);

    app.directive('menu', function () {
        return {
            restrict: 'E',
            controller: 'MenuCtrl',
            templateUrl:'/template/Directive/menu.html'
        };
    });

    app.directive('back', function() {
        return {
            restrict: 'E',
            scope: {
                url: '@'
            },
            controller: 'BackCtrl',
            templateUrl:'/template/Directive/back.html'
        };
    });

})();
