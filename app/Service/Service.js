(function() {

    var app = angular.module('app-service', []);

    app.service('storageService', ['$http', '$location', '$timeout', '$rootScope', function ($http, $location, $timeout, $rootScope) {
        var that = this;
        this.data = [];
        this.id = 0;

        this.findAll = function (callback) {
            that.sync(function() {
                if (typeof callback === "function") {
                    callback(that.data);
                }
            });
        };

        this.find = function (id, successCallback, failureCallback) {
            that.sync(function() {
                for (var i = 0; i < that.data.length; i++) {
                    var item = that.data[i];
                    if (item.id === parseInt(id)) {
                        if (typeof successCallback === "function") {
                            successCallback(item);
                            return false;
                        }
                    }
                }
                if (typeof failureCallback === "function") {
                    failureCallback();
                }
            });
        };

        this.sync = function(callback) {
            chrome.storage.sync.get('password', function(keys) {
                if (keys.password != null) {
                    that.data = keys.password;
                    $.each(that.data, function(index, value) {
                        that.id = (value.id > that.id) ? value.id : that.id;
                    });
                }
                if (typeof callback === "function") {
                    callback();
                }
            });
        };

        this.save = function(data, callback) {
            chrome.storage.sync.set({'password':data}, function() {
                if (typeof callback === "function") {
                    callback();
                }
            });
        };

        this.add = function (object, callback) {
            that.sync(function() {
                that.id++;
                var password = {
                    id: that.id,
                    data: object,
                    createdAt: new Date()
                };
                that.data.push(password);
                that.save(that.data);
                if (typeof callback === "function") {
                    callback(password.id);
                }
            });
        };

        this.remove = function(password) {
            this.data.splice(this.data.indexOf(password), 1);
            this.sync();
        };

        this.removeAll = function() {
            this.data = [];
            this.sync();
        };
    }]);

    app.factory('commonService', [function() {

        return {
            getDomainName: function(url) {
                url = url.replace('http://www.', '');
                url = url.replace('http://', '');
                url = url.replace('https://www.', '');
                url = url.replace('https://', '');
                var n = url.indexOf('/');
                return url.substring(0, n != -1 ? n : url.length);
            },
            encrypt: function(password, passphrase) {
                return CryptoJS.AES.encrypt(password, passphrase).toString()
            },
            decrypt: function(hash, passphrase) {
                return CryptoJS.AES.decrypt(hash, passphrase);
            }
        };
    }]);

})();
