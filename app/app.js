(function(){

    var app = angular.module('app', ['ngRoute', 'app-directive', 'app-service', 'app-layout', 'app-introduction', 'app-option', 'app-password']);

    app.config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/', {templateUrl:'/template/intro.html', controller:'IntroductionCtrl'})
            .when('/option', {templateUrl:'/template/Password/option.html', controller:'OptionCtrl'})
            .when('/password', {templateUrl:'/template/Password/manage.html', controller:'PasswordManageCtrl'})
            .when('/password/add', {templateUrl:'/template/Password/add.html', controller:'PasswordAddCtrl'})
            .when('/password/:id/detail', {templateUrl:'/template/Password/detail.html', controller:'PasswordDetailCtrl'})
            .when('/password/:id/update', {templateUrl:'/template/Password/update.html', controller:'PasswordUpdateCtrl'})
            .when('/password/:id/delete', {templateUrl:'/template/Password/delete.html', controller:'PasswordDeleteCtrl'})
            .when('/password/generate', {templateUrl:'/template/Password/generate.html', controller:'PasswordGenerateCtrl'})

            .otherwise({redirectTo: '/'});

        // HTML5 mode
        $locationProvider.html5Mode(true);

    }]);

    app.filter('partialFilter', function() {
        return function(data, search) {
            if (angular.isDefined(search) && search.length > 0) {
                var filtered = [];
                angular.forEach(data, function(item) {
                    if (item.data && ((item.data.website && item.data.website.includes(search)) || (item.data.username && item.data.username.includes(search)))) {
                        filtered.push(item);
                    }
                });
                return filtered;
            } else {
                return data;
            }
        };
    });

})();